package HW4;

public class MatrixHW {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        System.out.println("Головна діагональ:");
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(matrix[i][i]);
            if (i < matrix.length - 1) {
                System.out.print(", ");
            }
        }

        System.out.println("\nПобічна діагональ:");
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(matrix[i][matrix.length - 1 - i]);
            if (i < matrix.length - 1) {
                System.out.print(", ");
            }
        }
    }
}
