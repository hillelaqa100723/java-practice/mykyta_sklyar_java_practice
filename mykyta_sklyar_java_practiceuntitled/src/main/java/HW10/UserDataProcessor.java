package HW10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class User {
    private String firstName;
    private String lastName;
    private int age;

    public User(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}

public class UserDataProcessor {

    public static Map<Integer, User> getDataFromFile(File dataFile) {
        Map<Integer, User> userData = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(dataFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                int id = Integer.parseInt(parts[0]);
                String firstName = parts[1];
                String lastName = parts[2];
                int age = Integer.parseInt(parts[3]);
                userData.put(id, new User(firstName, lastName, age));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userData;
    }

    public static User getDataById(Map<Integer, User> mapData, Integer id) {
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, User> mapData, String lastName) {
        int count = 0;
        for (User user : mapData.values()) {
            if (user.getLastName().equals(lastName)) {
                count++;
            }
        }
        return count;
    }

    public static List<User> getUsersAgeMoreThen(Map<Integer, User> mapData, int age) {
        List<User> result = new ArrayList<>();
        for (User user : mapData.values()) {
            if (user.getAge() > age) {
                result.add(user);
            }
        }
        return result;
    }

    public static Map<String, List<Integer>> findEqualUsers(Map<Integer, User> users) {
        Map<String, List<Integer>> equalUsers = new HashMap<>();
        Map<String, List<Integer>> tempMap = new HashMap<>();

        for (Map.Entry<Integer, User> entry : users.entrySet()) {
            String fullName = entry.getValue().getFirstName() + " " + entry.getValue().getLastName();
            tempMap.computeIfAbsent(fullName, k -> new ArrayList<>()).add(entry.getKey());
        }

        for (Map.Entry<String, List<Integer>> entry : tempMap.entrySet()) {
            if (entry.getValue().size() > 1) {
                equalUsers.put(entry.getKey(), entry.getValue());
            }
        }

        return equalUsers;
    }

    public static void main(String[] args) {
        File dataFile = new File("/Users/nicksklar/mykyta_sklyar_java_practice/mykyta_sklyar_java_practiceuntitled/src/main/java/HW10/data.csv");
        Map<Integer, User> userData = getDataFromFile(dataFile);

        // Test the functions
        System.out.println("User with ID 1: " + getDataById(userData, 1));
        System.out.println("Occurrences of 'Smith': " + getNumberOfOccurrences(userData, "Smith"));
        System.out.println("Users older than 30: " + getUsersAgeMoreThen(userData, 30));
        System.out.println("Equal users: " + findEqualUsers(userData));
    }
}
