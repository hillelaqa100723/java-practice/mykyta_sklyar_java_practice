package HW2;

public class PalindromeChecker {
    public static boolean isPalindrome(String str) {
        String cleanedStr = str.replaceAll("\\s", "").toLowerCase();

        int left = 0;
        int right = cleanedStr.length() - 1;

        while (left<right) {
            if (cleanedStr.charAt(left) != cleanedStr.charAt(right)) {
                return false;
            }
            left++;
            right--;

        }

        return true;
    }

    public static void main(String[] angs) {
        String input ="Чи в окуня є Янукович";
        boolean rez = isPalindrome(input);

        if (rez) {
            System.out.println("Чел, харош, це є паліндром.");
        } else {
            System.out.println("Чел, теряйся, це не є паліндромом.");
        }
    }
}
