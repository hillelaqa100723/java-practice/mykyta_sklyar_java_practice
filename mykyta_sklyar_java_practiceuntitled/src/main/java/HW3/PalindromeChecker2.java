package HW3;

import java.util.Arrays;

public class PalindromeChecker2 {
    public static boolean isPalindrome(int[] arr) {
        int length = arr.length;
        for (int i = 0; i < length / 2; i++) {
            if (arr[i] != arr[length - i - 1]) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 3, 2, 1};
        boolean res = isPalindrome(array);

        if (res) {
            System.out.println("Рядок \""+ Arrays.toString(array) +"\" Є паліндромом");
        } else {
            System.out.println("Рядок \""+ Arrays.toString(array) +"\" Не є паліндромом");
        }
    }
}


