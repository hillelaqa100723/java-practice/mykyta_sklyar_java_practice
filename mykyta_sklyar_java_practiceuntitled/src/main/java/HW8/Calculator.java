package HW8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

enum Operator {
    PLUS("+"),
    MINUS("-"),
    MULTIPLY("*"),
    DIVIDE("/");

    private final String symbol;

    Operator(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}

public class Calculator {
    public static void main(String[] args) {
        File dataFile = new File("/Users/nicksklar/mykyta_sklyar_java_practice/mykyta_sklyar_java_practiceuntitled/data.txt");
        List<String> inputData = getDataFromFile(dataFile);
        if (inputData != null) {
            Double result = calculate(inputData);
            if (result != null) {
                String resultString = prepareResultString(inputData, result);
                System.out.println(resultString);
            }
        }
    }

    public static List<String> getDataFromFile(File dataFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(dataFile))) {
            String line = reader.readLine();
            if (line == null || line.isEmpty()) {
                System.out.println("File is empty or doesn't contain valid data.");
                return null;
            }
            return List.of(line.split(",\\s*"));
        } catch (IOException e) {
            System.out.println("Error reading data from file.");
            return null;
        }
    }

    public static Double calculate(List<String> data) {
        Double result = null;
        if (data.size() % 2 != 1) {
            System.out.println("Input data format is invalid.");
            return null;
        }

        try {
            result = Double.parseDouble(data.get(0));
            for (int i = 1; i < data.size(); i += 2) {
                Operator operator = Operator.valueOf(data.get(i));
                Double operand = Double.parseDouble(data.get(i + 1));

                switch (operator) {
                    case PLUS:
                        result += operand;
                        break;
                    case MINUS:
                        result -= operand;
                        break;
                    case MULTIPLY:
                        result *= operand;
                        break;
                    case DIVIDE:
                        if (operand != 0) {
                            result /= operand;
                        } else {
                            System.out.println("Cannot divide by zero.");
                            return null;
                        }
                        break;
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid number format.");
            return null;
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid argument.");
            return null;
        }

        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        StringBuilder resultString = new StringBuilder();
        resultString.append(data.get(0));
        for (int i = 1; i < data.size(); i += 2) {
            resultString.append(Operator.valueOf(data.get(i)).getSymbol()).append(data.get(i + 1));
        }
        resultString.append("=").append(result);
        return resultString.toString();
    }
}
