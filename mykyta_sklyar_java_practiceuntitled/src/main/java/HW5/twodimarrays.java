package HW5;

public class twodimarrays {

    public static void main(String[] args) {
        int[][] array = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[] sums = sumsInRows(array);
        printResults(sums);
    }

    public static int[] sumsInRows(int[][] source) {
        int[] sums = new int[source.length];

        for (int i = 0; i < source.length; i++) {
            int sum = 0;
            for (int j = 0; j < source[i].length; j++) {
                sum += source[i][j];
            }
            sums[i] = sum;
        }

        return sums;
    }

    public static void printResults(int[] sums) {
        System.out.println("Результат:");
        for (int i = 0; i < sums.length; i++) {
            System.out.print(sums[i]);
            if (i < sums.length - 1) {
                System.out.print(", ");
            }
        }
    }
}
