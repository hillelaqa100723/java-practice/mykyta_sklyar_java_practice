package HW6;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Lottery {
    public static void main(String[] args) {
        List<Integer> winningNum = generateWinningNumbers();
        List<Integer> guessedNum = getUserGuesses();

        System.out.println("\n(っ ͡Ⓘ ⏥ ͡Ⓘ)っ Виграшні номери: " + winningNum);
        System.out.println("(っ ͡Ⓘ ⏥ ͡Ⓘ)っ Ваші номери: " + guessedNum);

        List<Integer> matchingNums = new ArrayList<>(guessedNum);
        matchingNums.retainAll(winningNum);

        if (!matchingNums.isEmpty()) {
            System.out.println("(っ＾▿＾) Господи, Ви перемогли ᕙ( ︡'︡益'︠)ง");
        } else {
            System.out.println("(ㆆ_ㆆ) Ви програли (ㆆ_ㆆ)");
        }
    }

    private static List<Integer> generateWinningNumbers() {
        List<Integer> winningNum = new ArrayList<>();
        Random random = new Random();
        while (winningNum.size() < 6) {
            int winning = random.nextInt(36) + 1;
            if (!winningNum.contains(winning)) {
                winningNum.add(winning);
            }
        }
        return winningNum;
    }

    private static List<Integer> getUserGuesses() {
        Scanner scan = new Scanner(System.in);
        List<Integer> guessedNum = new ArrayList<>();

        System.out.println("(⊙.⊙(☉̃ₒ☉)⊙.⊙) Лотерея (⊙.⊙(☉̃ₒ☉)⊙.⊙)");
        System.out.println("(ɔ◔‿◔)ɔ Введіть 6 чисел, обираючи числа між 1 та 36: ");

        for (int i = 0; i < 6; i++) {
            System.out.println("(っ ͡❛ ͜ʖ ͡❛)っ Числа які ви обрали: " + guessedNum);
            System.out.println("(ɔ◔‿◔)ɔ Введіть число від 1 до 36: ");

            while (true) {
                try {
                    int number = Integer.parseInt(scan.nextLine());
                    if (number >= 1 && number <= 36) {
                        guessedNum.add(number);
                        break;
                    } else {
                        System.out.println("(っ ͡❛ ︹ ͡❛)っ " + number +
                                " Число не в проміжку між 1 та 36. Спробуйте ще раз!");
                    }
                } catch (NumberFormatException nfe) {
                    System.out.println("(っ ͡Ⓘ ︹ ͡Ⓘ)っ Так не піде, це не число. Спробуйте ще раз");
                }
            }
        }

        scan.close();
        return guessedNum;
    }
}
